$ ->
  if $('#map').length > 0
    if navigator.geolocation
      navigator.geolocation.getCurrentPosition (position) ->
        
        
        center = 
          lat: position.coords.latitude
          lng: position.coords.longitude

        map = new (google.maps.Map)(document.getElementById('map'),
          center: center
          minZoom: 2
          zoom: 2
          disableDefaultUI: true
          mapTypeId: google.maps.MapTypeId.ROADMAP)

        markers = [] 
        placecountrymarker = {}
        placecitymarker = {}

        infowindow = new (google.maps.InfoWindow)(maxWidth: 200)

        i = 0

        while i < data.length
          latLng = new (google.maps.LatLng)(data[i].latitude, data[i].longitude)
          marker = new (google.maps.Marker)(
            'position': latLng
            icon: data[i].avatar_url)
          if !placecountrymarker.hasOwnProperty(data[i].country)
            placecountrymarker[data[i].country] = []
          if !placecitymarker.hasOwnProperty(data[i].city)
            placecitymarker[data[i].city] = []
          placecountrymarker[data[i].country].push marker
          placecitymarker[data[i].city].push marker
          #markers.push(marker);
          google.maps.event.addListener marker, 'click', do (marker, i) ->
            ->
              infowindow.setContent data[i].email
              infowindow.open map, marker
              return
          ++i
         
        for country of placecountrymarker
          markerCluster = new MarkerClusterer(map, placecountrymarker[country],
            maxZoom: 4
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m')

        for city of placecitymarker
          markerCluster = new MarkerClusterer(map, placecitymarker[city],
            maxZoom: 10
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m')
        return

  $('#ct-btn').click ->
    $('#map').toggleClass 'clicked'
    return
  return

