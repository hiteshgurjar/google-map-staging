class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
 	
 	geocoded_by :address
  	reverse_geocoded_by :latitude, :longitude 
	after_validation :geocode, :if => :address_changed?





  	validates :address, presence: true
	validates :avatar, attachment_presence: true
	#has_attached_file :avatar, styles: { medium: "300x300>", thumb: "40x40>" }, default_url: "/images/:style/missing.png"


	if Rails.env.production?
  	has_attached_file :avatar,
		  :storage => :s3,
		  :s3_credentials => "#{Rails.root}/config/s3.yml",
		  :path => "/:class/avatars/:id/:style/:filename",
		  :url => ":s3_domain_url",
		  :styles => {medium: "300x300>", thumb: "40x40>"}
	else
		  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "40x40>" }, default_url: "/images/:style/missing.png"
	end
  
  	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  	def city
  	   address = Geocoder.search("#{self.latitude},#{self.longitude}").first
       city = address.present? ? address.city : ""
  	end
    def country
       address = Geocoder.search("#{self.latitude},#{self.longitude}").first
       country = address.present? ? address.country : ""
    end

    def avatar_url
        self.avatar.url(:thumb)
    end


end


