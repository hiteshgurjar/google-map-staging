Rails.application.routes.draw do
  
  #resource :users

  root 'home#index'

  devise_for :users, :controllers => { registrations: 'users/registrations' ,sessions: 'users/sessions'}
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	resource :users, :only => [:show]    
end
